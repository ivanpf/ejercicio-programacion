import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Esta clase sirve para definir los statics que usaremos durante el programa
 * 
 * @author Iván
 * @version 1.0
 */

public class JavaDoc_Buscaminas {
	static Scanner sc = new Scanner(System.in);
	static Random r = new Random();
	static ArrayList<String> Winners = new ArrayList<>();
	static String jugador;
	static int minas = 0;

	/**
	 * En esta clase vamos a empezar nuestro programa, como este programa esta hecho
	 * por funciones aqui solo estará el menu de nuestro juego, el cual llama a
	 * otras funciones
	 */
	public static void main(String[] args) {
		int f = 0;
		int c = 0;
		boolean fi = false;

		while (!fi) {
			int op = menu();
			switch (op) {
			case 1:
				System.out.println();
				System.out.println(
						"Buscaminas (en inglés: Minesweeper) es un videojuego para un jugador inventado en 1989. El juego ha sido programado para muchos sistemas operativos, \n pero debe su popularidad a las versiones que vienen con Microsoft Windows desde su versión 3.1.");
				System.out.println();
				System.out.println(
						"Nivel personalizado: en este caso el usuario personaliza su juego eligiendo el número de minas y el tamaño de la cuadricula");
				System.out.println();
				break;
			case 2:
				jugador = obtenirJugador("");
				System.out.println("Escribe el tamaño del mapa que quieres");
				System.out.print("Filas:");
				f = sc.nextInt();
				System.out.print("Columnas");
				c = sc.nextInt();
				System.out.println("Que cantidad de minas quieres?");
				minas = sc.nextInt();
				break;
			case 3:

				int[][] visual = new int[f][c];
				int[][] oculta = new int[visual.length][visual.length];
				boolean win = false;

				iniciarOculta(oculta);
				PonerMinas(oculta, minas);
				init(visual);

				while (!win) {

					imprimirVisual(visual);
					System.out.println();

					System.out.println();
					System.out.println("Dime una fila: ");
					int fil = sc.nextInt();
					System.out.println("Dime una columna: ");
					int col = sc.nextInt();
					destapar(visual, oculta, fil, col);
//					imprimirVisual(visual);

					win = FinDelJuego(visual, oculta, fil, col, minas, jugador);

				}
				break;
			case 4:
				System.out.println("Esta es la lista de los supervivientes de siria: ");
				System.out.println(Winners.toString().replace("[", "").replace("]", "").replace(",", ""));

				break;
			case 0:
				fi = true;
			}
		}

		System.out.println("A reveure!!");

	}

	/**
	 * La funcion de menu es la funcion que se encarga de que podamos elelgir una
	 * opción gracias a diferentes sysos
	 * 
	 * @param opcio Almacena nuestra respuesta con un int
	 * @return devuelve el numero que hemos introducido en opcio
	 */

	private static int menu() {
		int opcio = 0;

		System.out.println("***** BUSCAMINAS - MENU PRINCIPAL*****");
		System.out.println("ESCULL UNA OPCIO:");
		System.out.println("1- Veure instruccions");
		System.out.println("2- Cofiguracio");
		System.out.println("3- Jugar");
		System.out.println("4- Ver los ganadores");
		System.out.println("0- Sortir");
		opcio = sc.nextInt();
		sc.nextLine();
		return opcio;
	}

	/**
	 * Esta funcion sirve para imprimir la matriz de nombre "visual", gracias a 2
	 * for, los cuales recorren las filas "f" y las columnas "c" de la matriz
	 * 
	 * @param visual
	 * 
	 */
	public static void imprimirVisual(int[][] visual) {
		for (int i = 0; i < visual.length; i++) {
			for (int j = 0; j < visual[0].length; j++) {
				System.out.print(visual[i][j] + " ");
			}
			System.out.println();
		}
	}

	/**
	 * Esta funcion sirve para imprimir la matriz de nombre "oculta", gracias a 2
	 * for, los cuales recorren las filas "f" y las columnas "c" de la matriz
	 * 
	 * @param oculta
	 * 
	 */
	public static void imprimirOculta(int[][] oculta) {
		for (int i = 0; i < oculta.length; i++) {
			for (int j = 0; j < oculta[0].length; j++) {
				System.out.print(oculta[i][j] + " ");
			}
			System.out.println();
		}
	}

	/**
	 * Esta funcion sirve para iniciar la matriz de nombre "oculta", la cual vamos a
	 * llenar de 0 recorriendola con 2 for
	 * 
	 * @param oculta
	 * 
	 */
	private static void iniciarOculta(int[][] oculta) {
		for (int i = 0; i < oculta.length; i++) {
			for (int j = 0; j < oculta[i].length; j++) {
				oculta[i][j] = 0;
			}
		}
	}

	/**
	 * Esta funcion sirve para iniciar la matriz de nombre "visual", la cual vamos a
	 * llenar de 9 recorriendola con 2 for
	 * 
	 * @param visual
	 * 
	 */
	private static void init(int[][] visual) {
		for (int i = 0; i < visual.length; i++) {
			for (int j = 0; j < visual[i].length; j++) {
				visual[i][j] = 9;

			}
		}
	}

	/**
	 * Esta funcion nos sirve para obtener el nombre de un jugador
	 * 
	 * @param nom     variable donde vamos a guardar el nombre
	 * @param jugador El jugador el cual vamos a nombrar
	 * @return El nombre que hemos guardado en la variable nom
	 */
	private static String obtenirJugador(String jugador) {
		String nom;
		System.out.println("Indica el nom del " + jugador + " jugador");
		nom = sc.nextLine();
		return nom;
	}

	/**
	 * Esta función nos sirve para no salirnos de la matriz, y asi evitar que nos de
	 * error el programa
	 * 
	 * @param matriz La matriz que le pasamos
	 * @param h      las columnas
	 * @param v      las filas
	 * @return true o false, dependiendo de si nos salimos fuera de la matriz o no
	 */
	public static boolean estoyDentro(int[][] matriz, int h, int v) {

		if (h < 0 || v < 0 || h >= matriz.length || v >= matriz[0].length) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Esta funcion nos sirve para minar la matriz oculta, y las vamos a representar
	 * con el numero 1
	 * 
	 * @param oculta la matriz que el jugador no ve
	 * @param minas  el numero de minas que vamos a poner en el tablero
	 */
	public static void PonerMinas(int[][] oculta, int minas) {
		for (int i = 0; i < minas; i++) {
			int fil = r.nextInt(0, oculta.length);
			int col = r.nextInt(0, oculta.length);
			for (int j = 0; j < oculta.length; j++) {
				for (int j2 = 0; j2 < oculta[0].length; j2++) {
					oculta[fil][col] = 1;
				}
			}
		}

	}

	/**
	 * Esta funcion nos sirve para indicarnos el fin del juego, sea porque hemos
	 * explotado una mina, o porque hemos descubierto todo el mapa sin descubrir
	 * ninguna, aparte tambien añadiremos el nombre del jugador a la lista de ganadores
	 * 
	 * @param visual la matriz que el jugador no ve
	 * @param oculta  el numero de minas que vamos a poner en el tablero
	 * @param x Las columnas de la matriz
	 * @param y Las filas de la columna
	 * @param minas El numero de minas que hemos puesto
	 * @param nom El nombre del jugador
	 * 
	 */
	public static boolean FinDelJuego(int[][] visual, int[][] oculta, int x, int y, int minas, String nom) {

		int contador = 0;

		if (oculta[x][y] == 1) {

			System.out.println("BOOOOOM");
			return true;
		}

		for (int i = 0; i < visual.length; i++) {
			for (int j = 0; j < visual[0].length; j++) {

				if (visual[i][j] == 9) {

					contador++;
				}

			}
		}

		if (contador == minas) {

			System.out.println("Felicidades, te has pasado Siria");
			añadirWinner(Winners, nom);
			return true;
		}

		return false;

	}
	/**
	 * Esta funcion nos sirve para destapar una posicion de la matriz visual, aparte de que llamamos a una función mas para hacer el juego recursivo
	 * 
	 * @param visual la matriz visual 
	 * @param oculta  La matriz oculta 
	 * @param x Las columnas de la matriz
	 * @param y Las filas de la matriz
	 */
	public static void destapar(int[][] visual, int[][] oculta, int x, int y) {

		if (!estoyDentro(oculta, x, y))
			return;
		if (visual[x][y] != 9)
			return;

		while (true) {

			int number = descubrir(oculta, x, y);

			if (number == 0) {
				visual[x][y] = 0;
				for (int i = x - 1; i <= x + 1; i++) {
					for (int j = y - 1; j <= y + 1; j++) {
						destapar(visual, oculta, i, j);
					}
				}
				break;
			} else {
				visual[x][y] = number;
				break;
			}

		}

	}
	/**
	 * Esta funcion nos sirve para la recursividad del juego y asi "expandir" el destapado 
	 * 
	 * @param oculta  La matriz oculta 
	 * @param x Las columnas de la matriz
	 * @param y Las filas de la matriz
	 * 
	 * @return contador
	 */
	public static int descubrir(int[][] oculta, int x, int y) {

		int contador = 0;

		for (int i = x - 1; i <= x + 1; i++) {
			for (int j = y - 1; j <= y + 1; j++) {
				if (estoyDentro(oculta, i, j) && oculta[i][j] == 1)
					contador++;
			}
		}
		return contador;
	}
	/**
	 * Esta funcion nos sirve para añadir un nombte a una lista de ganadores
	 * 
	 * @param Winner La lista en la que guardamos los ganadores 
	 * @param nom El nombre del jugador ganador

	 */
	public static void añadirWinner(ArrayList<String> Winner, String nom) {
		Winner.add(nom);
	}

}
