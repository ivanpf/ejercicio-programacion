import java.util.Scanner;

public class Distancia_euclidiana {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		double x1, y1, x2, y2;
		double distancia;

		System.out.println("Punto 1");
		System.out.print("X: ");
		x1 = sc.nextInt();
		System.out.print("Y: ");
		y1 = sc.nextInt();

		System.out.println("Punto 2");
		System.out.print("X: ");
		x2 = sc.nextInt();
		System.out.print("Y: ");
		y2 = sc.nextInt();
		
		

		distancia = distancia_euclidea(x1, y1, x2, y2);
		System.out.println("La distancia euclidea es: " + distancia);
	}

	static double distancia_euclidea(double x1, double y1, double x2, double y2) {

		return Math.round(Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2)));
	}
}
