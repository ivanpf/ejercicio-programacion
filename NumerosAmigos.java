import java.util.*;

public class NumerosAmigos {

	public static void main(String[] args) {

		int numero1, numero2;
		Scanner sc = new Scanner(System.in);
		System.out.print("Introduce primer número: ");
		numero1 = sc.nextInt();
		System.out.print("Introduce segundo número: ");
		numero2 = sc.nextInt();

		amigos(numero1, numero2);

	}

	public static void amigos(int numero1, int numero2) {

		if (numero1 < 0 || numero2 < 0) {
			System.out.println("No puedes poner numeros negativos");
		} else {

			int suma = 0;
			for (int i = 1; i < numero1; i++) {
				if (numero1 % i == 0) {
					suma = suma + i;
				}
			}
			if (suma == numero2) {
				suma = 0;
				for (int i = 1; i < numero2; i++) {
					if (numero2 % i == 0) {
						suma = suma + i;
					}
				}
				if (suma == numero1) {
					System.out.println("Son besties");
				} else {
					System.out.println("Definitivamente, no son besties");
				}
			} else {
				System.out.println("Definitivamente, no son besties");
			}
		}
	}
}
