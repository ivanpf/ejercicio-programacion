import java.util.*;

public class Factorial_Recursivo {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int num;
		long resultado;
		System.out.print("Dame un nombre: ");
		num = sc.nextInt();

		if (num < 0)
			System.out.println("Pon numeros positivos");
		else {

			resultado = factorial(num);
			System.out.println(" El factorial es: " + resultado);
		}

	}

	static long factorial(int num) {
		long res;
		if (num == 0) //
			res = 1;
		else
			res = num * factorial(num - 1);
		return (res);

	}
}